package com.hiddenhalfangel.mocks.exceptions;

public class TechnicalException extends MockException {

    private static final long serialVersionUID = 1L;

    public TechnicalException(String message) {
        super(message);
    }
}
