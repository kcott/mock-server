package com.hiddenhalfangel.mocks.beans.webservices.inputs;

import lombok.Getter;
import lombok.Setter;

public class BodyMockInput {
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private Long urlMockId;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private String description;

    @Getter
    @Setter
    private String requestMethod;

    @Getter
    @Setter
    private Boolean active;

    @Getter
    @Setter
    private String requestBody;

    @Getter
    @Setter
    private String responseBody;
}
