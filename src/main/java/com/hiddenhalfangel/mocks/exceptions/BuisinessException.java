package com.hiddenhalfangel.mocks.exceptions;

public class BuisinessException extends MockException {

    private static final long serialVersionUID = 1L;

    public BuisinessException(String message) {
        super(message);
    }
}
