package com.hiddenhalfangel.mocks.exceptions;

public abstract class MockException extends Exception {

    private static final long serialVersionUID = 1L;

    public MockException(String message) {
        super(message);
    }
}
