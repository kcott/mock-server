package com.hiddenhalfangel.mocks.beans.databases;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.Setter;

@Entity
@DynamicUpdate
@Table(schema = "mocks",
       indexes = {
           @Index(name = "IX_UrlMock_name", columnList = "name", unique = false)
       }
)
@SequenceGenerator(name = "SEQ_UrlMock", schema = "mocks")
public class UrlMock {
    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_UrlMock")
    private Long id;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private String url;

    @Getter
    @Setter
    private String description;

    @Getter
    @Setter
    private Boolean active;

    @Getter
    @Setter
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "url_mock_id")
    private List<BodyMock> bodyMocks = new ArrayList<>();

    @PrePersist
    @PreUpdate
    private void prePersist() {
        if (active == null) {
            active = Boolean.FALSE;
        }
    }
}
