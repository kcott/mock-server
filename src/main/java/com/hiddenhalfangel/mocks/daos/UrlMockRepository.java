package com.hiddenhalfangel.mocks.daos;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.hiddenhalfangel.mocks.beans.databases.UrlMock;

public interface UrlMockRepository extends CrudRepository<UrlMock, Long> {
    @Query(value = "select bm.response_body " +
            "from mocks.url_mock um " +
            "inner join mocks.body_mock bm on um.id = bm.url_mock_id " +
            "where um.url = :url " +
            "  and cast(bm.request_body as jsonb) = cast(:requestBody as jsonb) " +
            "  and bm.request_method = :requestMethod " +
            "  and um.active = true " +
            "  and bm.active = true ",
            nativeQuery = true
            )
    String find(@Param("url") String url, @Param("requestMethod") String requestMethod, @Param("requestBody") String requestBody);

    @Query(value = "select bm.response_body " +
            "from mocks.url_mock um " +
            "inner join mocks.body_mock bm on um.id = bm.url_mock_id " +
            "where um.url = :url " +
            "  and bm.request_method = :requestMethod " +
            "  and um.active = true " +
            "  and bm.active = true ",
            nativeQuery = true
            )

    String find(@Param("url") String url, @Param("requestMethod") String requestMethod);

    @Modifying
    @Query(value = "update UrlMock um set " +
            "um.active = false " +
            "where um.url = :url "
            )
    int deactivated(@Param("url") String url);

    @Modifying
    @Query(value = "update UrlMock um set " +
            "um.name = :name, " +
            "um.url = :url, " +
            "um.description = :description, " +
            "um.active = :active " +
            "where um.id = :id "
            )
    int update(@Param("id") long id, @Param("name") String name, @Param("url") String url, @Param("active") boolean active, @Param("description") String description);
}
