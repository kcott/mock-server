package com.hiddenhalfangel.mocks.beans.databases;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.hiddenhalfangel.mocks.enums.RequestMethodMockEnum;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(schema = "mocks")
@SequenceGenerator(name = "SEQ_BodyMock", schema = "mocks")
public class BodyMock {
    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_BodyMock")
    private Long id;

    @Getter
    @Setter
    @Column(name = "url_mock_id")
    private Long urlMockId;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private String description;

    @Getter
    @Setter
    @Enumerated(EnumType.STRING)
    private RequestMethodMockEnum requestMethod;

    @Getter
    @Setter
    private Boolean active;

    @Getter
    @Setter
    private String requestBody;

    @Getter
    @Setter
    private String responseBody;

    @PrePersist
    @PreUpdate
    private void prePersist() {
        if (active == null) {
            active = Boolean.FALSE;
        }
    }
}
