package com.hiddenhalfangel.mocks.beans.mappers;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.ValueMapping;

import com.hiddenhalfangel.mocks.beans.databases.BodyMock;
import com.hiddenhalfangel.mocks.beans.databases.UrlMock;
import com.hiddenhalfangel.mocks.beans.webservices.inputs.BodyMockInput;
import com.hiddenhalfangel.mocks.beans.webservices.inputs.UrlMockInput;
import com.hiddenhalfangel.mocks.beans.webservices.outputs.BodyMockOutput;
import com.hiddenhalfangel.mocks.beans.webservices.outputs.UrlMockOutput;

@Mapper(componentModel = "spring")
public interface UrlMapper {
    public UrlMock toUrlMock(UrlMockInput url);

    @ValueMapping(source = "requestMethod.name", target = "requestMethod")
    public BodyMock toBodyMock(BodyMockInput body);

    @ValueMapping(source = "requestMethod", target = "requestMethod.name")
    public BodyMockOutput toBodyMockOutput(BodyMock body);

    public UrlMockOutput toUrlMockOutput(UrlMock url);

    public List<UrlMockOutput> toUrlMockOutput(Iterable<UrlMock> url);
}
