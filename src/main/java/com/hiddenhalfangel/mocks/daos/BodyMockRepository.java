package com.hiddenhalfangel.mocks.daos;

import org.springframework.data.repository.CrudRepository;

import com.hiddenhalfangel.mocks.beans.databases.BodyMock;

public interface BodyMockRepository extends CrudRepository<BodyMock, Long> {

}
