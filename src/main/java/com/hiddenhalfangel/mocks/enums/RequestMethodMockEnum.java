package com.hiddenhalfangel.mocks.enums;

import org.springframework.util.StringUtils;

import com.hiddenhalfangel.mocks.exceptions.BuisinessException;
import com.hiddenhalfangel.mocks.utils.MessageUtils;

public enum RequestMethodMockEnum {
    DELETE,
    GET,
    PATCH,
    POST,
    PUT;

    public static RequestMethodMockEnum get(String verb) throws BuisinessException {
        if (StringUtils.isEmpty(verb)) {
            throw new BuisinessException(MessageUtils.REQUEST_METHOD_VERB_EMPTY);
        }
        switch(verb.toUpperCase()) {
            case "DELETE":
                return RequestMethodMockEnum.DELETE;
            case "GET":
                return RequestMethodMockEnum.GET;
            case "PATCH":
                return RequestMethodMockEnum.PATCH;
            case "POST":
                return RequestMethodMockEnum.POST;
            case "PUT":
                return RequestMethodMockEnum.PUT;
            default:
                throw new BuisinessException(String.format(MessageUtils.REQUEST_METHOD_VERB_UNKNOW, verb));
        }
    }
}
