package com.hiddenhalfangel.mocks.enums;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hiddenhalfangel.mocks.exceptions.BuisinessException;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class RequestMethodMockEnumTest {
    private static final String DELETE = "delete";
    private static final String GET = "get";
    private static final String PATCH = "patch";
    private static final String POST = "post";
    private static final String PUT = "put";

    @Test
    public void getTest() throws BuisinessException {
        assertThrows(BuisinessException.class, () -> RequestMethodMockEnum.get(null));
        assertThrows(BuisinessException.class, () -> RequestMethodMockEnum.get("not exist"));

        assertEquals(RequestMethodMockEnum.DELETE, RequestMethodMockEnum.get(DELETE));
        assertEquals(RequestMethodMockEnum.GET, RequestMethodMockEnum.get(GET));
        assertEquals(RequestMethodMockEnum.PATCH, RequestMethodMockEnum.get(PATCH));
        assertEquals(RequestMethodMockEnum.POST, RequestMethodMockEnum.get(POST));
        assertEquals(RequestMethodMockEnum.PUT, RequestMethodMockEnum.get(PUT));
    }
}
