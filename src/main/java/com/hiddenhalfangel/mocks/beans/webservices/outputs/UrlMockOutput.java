package com.hiddenhalfangel.mocks.beans.webservices.outputs;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class UrlMockOutput {
    @Getter
    @Setter
    private String id;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private String url;

    @Getter
    @Setter
    private String description;

    @Getter
    @Setter
    private Boolean active;

    @Getter
    @Setter
    private List<BodyMockOutput> bodyMocks;
}
