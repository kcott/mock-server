package com.hiddenhalfangel.mocks.services.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.hiddenhalfangel.mocks.beans.databases.BodyMock;
import com.hiddenhalfangel.mocks.beans.databases.UrlMock;
import com.hiddenhalfangel.mocks.daos.BodyMockRepository;
import com.hiddenhalfangel.mocks.daos.UrlMockRepository;
import com.hiddenhalfangel.mocks.exceptions.BuisinessException;
import com.hiddenhalfangel.mocks.exceptions.MockException;
import com.hiddenhalfangel.mocks.services.MockService;
import com.hiddenhalfangel.mocks.utils.MessageUtils;

@Transactional(rollbackFor = MockException.class)
@Service(value = "mockService")
public class MockServiceImpl implements MockService {
    private UrlMockRepository urlMockRepository;
    private BodyMockRepository bodyMockRepository;

    public MockServiceImpl(UrlMockRepository urlMockRepository, BodyMockRepository bodyMockRepository) {
        this.urlMockRepository = urlMockRepository;
        this.bodyMockRepository = bodyMockRepository;
    }

    @Override
    public String getResponseBody(String url, String requestMethod, String body) throws MockException {
        if (StringUtils.isEmpty(url) && StringUtils.isEmpty(requestMethod)) {
            throw new BuisinessException(MessageUtils.EMPTY_URL_AND_REQUEST_METHOD);
        } else if (StringUtils.isEmpty(url)) {
            throw new BuisinessException(MessageUtils.EMPTY_URL);
        } else if (StringUtils.isEmpty(requestMethod)) {
            throw new BuisinessException(MessageUtils.EMPTY_REQUEST_METHOD);
        }
        if ("POST".equals(requestMethod) || "PUT".equals(requestMethod)) {
            if (StringUtils.isEmpty(body)) {
                throw new BuisinessException(MessageUtils.EMPTY_BODY);
            }
            return urlMockRepository.find(url, requestMethod, body);
        } else {
            return urlMockRepository.find(url, requestMethod);
        }
    }
    @Override
    public UrlMock saveUrl(UrlMock urlMock) throws MockException {
        return urlMockRepository.save(urlMock);
    }
    @Override
    public UrlMock updateUrl(UrlMock urlMock) throws MockException {
        if (urlMock.getActive() != null && urlMock.getActive()) {
            urlMockRepository.deactivated(urlMock.getUrl());
        }
        if (urlMockRepository.update(urlMock.getId(), urlMock.getName(), urlMock.getUrl(), urlMock.getActive(), urlMock.getDescription()) == 0) {
            throw new BuisinessException(MessageUtils.UPDATE_NO_RESULT);
        }
        return urlMockRepository.findById(urlMock.getId()).get();
    }
    @Override
    public BodyMock saveUrlBody(BodyMock bodyMock) throws MockException {
        return bodyMockRepository.save(bodyMock);
    }
    @Override
    public void deleteUrl(long id) throws MockException {
        urlMockRepository.deleteById(id);
    }
    @Override
    public void deleteUrlBody(long id) throws MockException {
        bodyMockRepository.deleteById(id);
    }
    @Override
    public Iterable<UrlMock> findAllUrlMocks() throws MockException {
        return urlMockRepository.findAll();
    }

}
