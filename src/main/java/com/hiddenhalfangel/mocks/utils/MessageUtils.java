package com.hiddenhalfangel.mocks.utils;

public abstract class MessageUtils {
    public static final String EMPTY_REQUEST_METHOD = "empty request method";
    public static final String EMPTY_BODY = "empty body";
    public static final String EMPTY_URL = "empty url";
    public static final String EMPTY_URL_AND_REQUEST_METHOD = "empty url and request method";
    public static final String UPDATE_NO_RESULT = "No data updating";
    public static final String URL_NOT_FOUND = "url not found";
    public static final String URLS_NOT_FOUND = "urls not found";
    public static final String REQUEST_METHOD_VERB_EMPTY = "Request method verb cannot be empty";
    public static final String REQUEST_METHOD_VERB_UNKNOW = "Request method verb '%s' cannot be empty";

    private MessageUtils() {
    }
}
