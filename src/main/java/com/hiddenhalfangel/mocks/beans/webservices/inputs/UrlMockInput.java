package com.hiddenhalfangel.mocks.beans.webservices.inputs;

import lombok.Getter;
import lombok.Setter;

public class UrlMockInput {
    @Getter
    @Setter
    private String id;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private String url;

    @Getter
    @Setter
    private String description;

    @Getter
    @Setter
    private Boolean active;

}
