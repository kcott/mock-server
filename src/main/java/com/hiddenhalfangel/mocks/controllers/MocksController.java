package com.hiddenhalfangel.mocks.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hiddenhalfangel.mocks.beans.databases.BodyMock;
import com.hiddenhalfangel.mocks.beans.databases.UrlMock;
import com.hiddenhalfangel.mocks.beans.mappers.UrlMapper;
import com.hiddenhalfangel.mocks.beans.webservices.inputs.BodyMockInput;
import com.hiddenhalfangel.mocks.beans.webservices.inputs.UrlMockInput;
import com.hiddenhalfangel.mocks.beans.webservices.outputs.BodyMockOutput;
import com.hiddenhalfangel.mocks.beans.webservices.outputs.UrlMockOutput;
import com.hiddenhalfangel.mocks.exceptions.MockException;
import com.hiddenhalfangel.mocks.services.MockService;

import lombok.extern.log4j.Log4j2;

@Controller
@CrossOrigin(origins = "*")
@RequestMapping(value = "/mocks")
@Log4j2
public class MocksController {
    private static Logger logger = LogManager.getLogger();

    private UrlMapper urlMapper = Mappers.getMapper(UrlMapper.class);

    private MockService mockService;

    @Autowired
    public MocksController(MockService mockService) {
        this.mockService = mockService;
    }

    @RequestMapping(
            value = "/mock/{url}",
            method = {
                    RequestMethod.DELETE,
                    RequestMethod.GET,
                    RequestMethod.PATCH,
                    RequestMethod.POST,
                    RequestMethod.PUT
            })
    public @ResponseBody String getDispatcher(
            @PathVariable(name = "url") String url,
            HttpServletRequest request,
            @RequestBody(required = false) String body) throws MockException {
        return mockService.getResponseBody(url, request.getMethod(), body);
    }

    @GetMapping(value = "/urls")
    public @ResponseBody List<UrlMockOutput> getUrls() throws MockException {
        Iterable<UrlMock> urlMocks = mockService.findAllUrlMocks();
        return urlMapper.toUrlMockOutput(urlMocks);
    }

    @PutMapping(value = "/url/add")
    public @ResponseBody UrlMockOutput addUrl(@RequestBody UrlMockInput urlMockInput) throws MockException {
        UrlMock urlMock = urlMapper.toUrlMock(urlMockInput);
        urlMock = mockService.saveUrl(urlMock);
        return urlMapper.toUrlMockOutput(urlMock);
    }

    @PostMapping(value = "/url/update")
    public @ResponseBody UrlMockOutput updateUrl(@RequestBody UrlMockInput urlMockInput) throws MockException {
        UrlMock urlMock = urlMapper.toUrlMock(urlMockInput);
        urlMock = mockService.updateUrl(urlMock);
        return urlMapper.toUrlMockOutput(urlMock);
    }

    @DeleteMapping(value = "/url/delete/{id}")
    public void deleteUrl(@PathVariable(name = "id") long id) throws MockException {
        mockService.deleteUrl(id);
    }

    @PutMapping(value = {"/url/body/add", "/url/body/update"})
    public @ResponseBody BodyMockOutput addUrlBody(@RequestBody BodyMockInput bodyMockInput) throws MockException {
        BodyMock bodyMock = urlMapper.toBodyMock(bodyMockInput);
        bodyMock = mockService.saveUrlBody(bodyMock);
        return urlMapper.toBodyMockOutput(bodyMock);
    }

    @DeleteMapping(value = "/url/body/delete/{id}")
    public void deleteUrlBody(@PathVariable(name = "id") long id) throws MockException {
        mockService.deleteUrlBody(id);
    }
}
