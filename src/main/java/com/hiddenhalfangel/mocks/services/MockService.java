package com.hiddenhalfangel.mocks.services;

import com.hiddenhalfangel.mocks.beans.databases.BodyMock;
import com.hiddenhalfangel.mocks.beans.databases.UrlMock;
import com.hiddenhalfangel.mocks.exceptions.MockException;

public interface MockService {
    String getResponseBody(String url, String requestMethod, String body) throws MockException;

    UrlMock saveUrl(UrlMock urlMock) throws MockException;

    UrlMock updateUrl(UrlMock urlMock) throws MockException;

    BodyMock saveUrlBody(BodyMock urlMock) throws MockException;

    void deleteUrl(long id) throws MockException;

    void deleteUrlBody(long id) throws MockException;

    Iterable<UrlMock> findAllUrlMocks() throws MockException;
}
