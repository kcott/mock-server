package com.hiddenhalfangel.mocks.services;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import com.hiddenhalfangel.mocks.daos.UrlMockRepository;
import com.hiddenhalfangel.mocks.exceptions.BuisinessException;
import com.hiddenhalfangel.mocks.exceptions.MockException;

@ActiveProfiles("test")
@ExtendWith(MockitoExtension.class)
@SpringBootTest
public class MockServiceTest
{
    private static final String emptyUrl = "";
    private static final String url = "url";
    private static final String emptyRequestMethod = "";
    private static final String empty_body = "";
    private static final String body = "{}";
    private static final String bodySuccess = "{'succes'}";
    private static final String[] requestMethods = {"GET", "DELETE", "PATCH", "POST", "PUT"};

    private MockService mockService;

    @MockBean
    private UrlMockRepository urlMockRepository;

    @Autowired
    public MockServiceTest(MockService mockService) {
        this.mockService = mockService;
    }

    @BeforeEach
    public void beforeEach() {
        when(urlMockRepository.find(emptyUrl, emptyRequestMethod, body)).thenReturn(bodySuccess);
        when(urlMockRepository.find(url, emptyRequestMethod, body)).thenReturn(bodySuccess);

        for (String requestMethod : requestMethods) {
            when(urlMockRepository.find(url, requestMethod, body)).thenReturn(bodySuccess);
            when(urlMockRepository.find(url, requestMethod)).thenReturn(bodySuccess);
        }
    }

    @Test
    public void getResponseBodyTest() throws MockException {
        // Empty URL tests
        for (String requestMethod : requestMethods) {
            assertThrows(BuisinessException.class, () -> mockService.getResponseBody(null, requestMethod, body));
            assertThrows(BuisinessException.class, () -> mockService.getResponseBody(emptyUrl, requestMethod, body));
        }

        // Empty request method tests
        assertThrows(BuisinessException.class, () -> mockService.getResponseBody(url, null, body));
        assertThrows(BuisinessException.class, () -> mockService.getResponseBody(url, emptyRequestMethod, body));

        // Empty body tests
        for (String requestMethod : requestMethods) {
            if ("POST".equals(requestMethod) || "PUT".equals(requestMethod)) {
                assertThrows(BuisinessException.class, () -> mockService.getResponseBody(url, requestMethod, null));
                assertThrows(BuisinessException.class, () -> mockService.getResponseBody(url, requestMethod, empty_body));
            } else {
                assertNotNull(mockService.getResponseBody(url, requestMethod, null), requestMethod);
                assertNotNull(mockService.getResponseBody(url, requestMethod, empty_body), requestMethod);
            }
        }
    }

}
